#require 'nokogiri'

class HomeController < ShopifyApp::AuthenticatedController
  #after_action :allow_facebook_iframe
  #before_action :set_event, only: [:index]


  def index

   user_shop = session[:shopify_domain]
   @root_url = request.base_url
    ##code for access shopify information
    #vars = request.query_parameters
    #user_shop = vars[:shop]
    @shop_exist = Shop.where(:shopify_domain => user_shop).first

    unless @shop_exist.setting.present?

      #check if setting id is blank then install webhoks this check mentioned for first time when app installed
      @insured_product_id = nil
     
      ## shopify app uninstall webhook
      app_uininstall_webhook(request.base_url)
      app_cart_create_webhook(request.base_url)
      app_cart_update_webhook(request.base_url)
      ##end uninstall webhook

      ## shopify api for create webhook create order
      webhook_url = request.base_url+"/webhookrequest"
      webhook = ShopifyAPI::Webhook.create(:format => "json", :topic => "orders/create", :address =>webhook_url)
      ## end shopify api for create webhook

      ##create insured product
      @bundled_variant_id = create_insured_product(@root_url)
      ##end to create insured product

      ##create insured product
      @opt_in_variant_id = create_insured_product_opt_in(@root_url)
      ##end to create insured product
      

      #Save into settings also
      settingObj = Setting.new
      settingObj.shop_id = @shop_exist.id
      #settingObj.shopify_store_name = @shop_exist.shopify_domain
      settingObj.activation = 1
      settingObj.bundled_variant_id = @bundled_variant_id
      settingObj.opt_in_variant_id = @opt_in_variant_id
      settingObj.opt_in_value = 2
      settingObj.is_bundle = 1
      settingObj.save

      @shop_exist.setting = settingObj

      #call function to add script of shopify ajax api in theme.liquid file 
      add_script_in_theme_liquid(@root_url)
     
      #end to call function
    end
     

    @setting_id = ''
    if @shop_exist.setting.present?
      @setting_id = @shop_exist.setting.id

      add_customjs_in_cart_template(@root_url,@shop_exist.setting,@bundled_variant_id,@opt_in_variant_id)

    end

    #puts "====>"+@orders.inspect
  end #end action

  def callback
    user_shop = session[:shopify_domain]
    #puts "===hello callback==>"+user_shop
  	render :json => "hello"
  end

  def all_orders
    @orders = ShopifyAPI::Order.all
    user_shop = session[:shopify_domain]
    @shop_exist = Shop.where(:shopify_domain => user_shop).first
    puts "=====orders======="+@shop_exist.inspect
  end

  

  def destroy
    @shop_exist = Shop.where(:id => params[:shop_id]).first

    if @shop_exist.subscription.present?
      obj = Subscription.where(:id => @shop_exist.subscription.id).first
      t=obj.update_attributes(:status => 'decline')
      
    end

    @recurring_application_charge = ShopifyAPI::RecurringApplicationCharge.find(params[:id])

    @recurring_application_charge.cancel

    flash[:notice] = "Recurring application charge was cancelled successfully"

    redirect_to_correct_path(@recurring_application_charge,params[:shop_id])


  end

private
  
  def redirect_to_correct_path(recurring_application_charge,shop_id)
    if recurring_application_charge.try(:capped_amount)
      redirect_to decline_charge_path(shop_id)

      #redirect_to controller: 'home', action: 'index'
    else
      redirect_to recurring_application_charge_path
    end
  end

  def app_uininstall_webhook(url)
    # create webhook when app is uninstalled
    unless "https".in? url
      url.gsub! 'http', 'https'
    end

    webhook = {
        topic: 'app/uninstalled',
        address: "#{url}/appUninstallWebHook",
        format: 'json'}
    ShopifyAPI::Webhook.create(webhook)
  end 


  def app_cart_create_webhook(url)
    # create webhook when app is uninstalled
    unless "https".in? url
      url.gsub! 'http', 'https'
    end

    webhook = {
        topic: 'carts/create',
        address: "#{url}/cartCreateWebHook",
        format: 'json'}
    ShopifyAPI::Webhook.create(webhook)
  end 

  def app_cart_update_webhook(url)
    # create webhook when app is uninstalled
    unless "https".in? url
      url.gsub! 'http', 'https'
    end

    webhook = {
        topic: 'carts/update',
        address: "#{url}/cartUpdateWebHook",
        format: 'json'}
    ShopifyAPI::Webhook.create(webhook)
  end 

  def create_insured_product(url) 
    unless "https".in? url
      url.gsub! 'http', 'https'
    end


    images = []
    image = {}
    image["src"] = url+'/kedge.jpg'
    images << image

     new_product = ShopifyAPI::Product.new({
        title:                'Kedge Lost Stolen Damaged',
        body_html:            '<strong>Kedge is an independent,third party service that replace or refunds right away if the unexpected happens. Get Kedge 100% guaranteed coverage for lost, stolen, or damaged deliveries. Yes even for theft!</strong>',
        vendor:               'insurance',
        product_type:         'insurance',
        tags:                 'insurance,insured',
        images:               images
      })

      new_product.save
      #puts "=========variants bundled==========>"+new_product.variants.inspect
      #return new_product.id
      return new_product.variants.first.id
      
  end

  def create_insured_product_opt_in(url) 
    unless "https".in? url
      url.gsub! 'http', 'https'
    end


    images = []
    image = {}
    image["src"] = url+'/kedge.jpg'
    images << image

     new_product = ShopifyAPI::Product.new({
        title:                'Kedge Lost Stolen Damaged',
        body_html:            '<strong>Kedge is an independent,third party service that replace or refunds right away if the unexpected happens. Get Kedge 100% guaranteed coverage for lost, stolen, or damaged deliveries. Yes even for theft!</strong>',
        vendor:               'insurance',
        product_type:         'insurance',
        tags:                 'insurance,insured',
        images:               images
      })

      new_product.save

      new_product.variants << ShopifyAPI::Variant.new(
      :title => "Kedge Lost Stolen Damaged",
      :sku => "",
      :option1 => "Kedge Lost Stolen Damaged",
      :price => 0.01,
      :inventory_policy => "deny",
      :inventory_management => '',
      :inventory_quantity => 0,
      :position => 1
      )
      new_product.save
      #puts "=========variants opt in==========>"+new_product.variants.inspect
      #return new_product.id
      new_variant_id = new_product.variants[1].id
      return new_variant_id

  end


  def add_script_in_theme_liquid(url)
      asset_theme = ShopifyAPI::Asset.find('layout/theme.liquid') 
      backup_theme = asset_theme.value

      string_index_theme = asset_theme.value.index("{{ content_for_header")
      string_index_theme  = (string_index_theme.to_i)-1
      snippet_theme = '<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  {{ "api.jquery.js" | shopify_asset_url | script_tag }}'
      #puts "==yess==>"+snippet_theme

      unless "api.jquery.js".in? asset_theme.value
        backup_theme.insert string_index_theme, snippet_theme
        asset_theme.value = backup_theme
        theme_save=asset_theme.save
      end
    
  end

  def add_customjs_in_cart_template(url,settingObj,bundled_variant_id,opt_in_variant_id)
      asset_theme = ShopifyAPI::Asset.find('sections/cart-template.liquid') 
      backup_theme = asset_theme.value

      string_index_theme = asset_theme.value.index("{% schema")

      
      snippet_theme_src = url+"/assets/custom.js?shop="+settingObj.shop.shopify_domain

      snippet_theme = '<script data-id="boughtshopId" data-bundled_variant_id="'+bundled_variant_id.to_s+'" data-bundled_opt_in_id="'+opt_in_variant_id.to_s+'" data-requesturl="'+url+'" data-shop="'+settingObj.shop.shopify_domain+'" data-is_bundle="'+settingObj.is_bundle.to_s+'" data-is_opt_in="'+settingObj.is_opt_in.to_s+'" src="'+snippet_theme_src+'"></script>'

      unless "custom.js".in? asset_theme.value
        string_index_theme  = (string_index_theme.to_i)-1

        backup_theme.insert string_index_theme, snippet_theme
        asset_theme.value = backup_theme
        theme_save=asset_theme.save
      end
    
  end



  # def allow_facebook_iframe
  #   response.headers['X-Frame-Options'] = 'ALLOW-FROM https://slavatest3118.myshopify.com'
  # end

end
