require 'net/http'
require 'json'
require 'net/smtp'
require 'csv'


class SettingsController < ApplicationController

  protect_from_forgery unless: -> { request.format.json? }

  skip_before_action :verify_authenticity_token, :only => [:fetch_product,:appUninstallWebHook,:cartCreateWebHook]

  #after_action :allow_iframe, only: :create

  def index



  end

  def new
    @setting = Setting.new
   
  end

  def create
    root_url = root_url(:only_path => false)
    unless "https".in? root_url
      root_url.gsub! 'http', 'https'
    end

    ids = params[:setting][:id]
    saved_data = Setting.where(id: ids).update_or_create(setting_params)

    if saved_data
      flash[:notice] = "Configuration saved successfully!"
      redirect_to controller: 'home', action: 'index'
      #redirect_to root_url
    else
      redirect_to controller: 'home', action: 'index'
      #redirect_to root_url
    end

  end

  def webhookrequest
    request.body.rewind
    jsonString = request.body.read
    json = JSON.parse(jsonString)

    #puts "====api==>"+json['id'].inspect

    #Write Order Log
    #my_file = File.new("order_log.txt","a+")
    #my_file.puts(jsonString)
    #my_file.close

    orderobj = Order.new
      #save order
    #orderobj.shop_id = @shop_exist.id
    #orderobj.shopify_store_name = @shop_exist.shopify_domain
    orderobj.creator_id = json['user_id']
    orderobj.order_id = json['id']
    orderobj.total_price = json['total_price']
    orderobj.total_tax = json['total_tax']
    #orderobj.total_shipping_cost =order.total_shipping_cost
    #orderobj.total_shipping_charged = order.total_shipping_charged
    orderobj.order_number = json['order_number']
    #orderobj.total_shipping_charged = order.total_shipping_charged
    orderobj.status = json['financial_status']
    orderobj.save

  	render :text => json

  end

  def appUninstallWebHook

    request.body.rewind
    jsonString = request.body.read
    json = JSON.parse(jsonString)

    domain = json['myshopify_domain']
    shop = Shop.where(:shopify_domain => domain).first
    if shop.present?

      #delete table data
      Shop.where(:id => shop.id).destroy_all
      Order.where(:shop_id => shop.id).destroy_all
      
      #end delete table data


    end

    render :json => jsonString

  end

  def cartCreateWebHook

    request.body.rewind
    jsonString = request.body.read
    json = JSON.parse(jsonString)

    domain = json['myshopify_domain']
    shop = Shop.where(:shopify_domain => domain).first
    puts "=======in cart create webhook========="
    render :json => shop
  end

  def cartUpdateWebHook

    request.body.rewind
    jsonString = request.body.read
    json = JSON.parse(jsonString)

    domain = json['myshopify_domain']
    shop = Shop.where(:shopify_domain => domain).first
    puts "=======in cart update webhook========="
    #puts "=========json======"+json.inspect
    #Shopify.addItem(17904498212928, 50);
    #render :json => shop
  end

  def check_shop_exist
     #for cross domain add this line
    headers['Access-Control-Allow-Origin'] = "*"
    vars = request.query_parameters
    shop_id = vars[:shop_id]
    shop_exist = Shop.where(:shopify_domain => shop_id).first

    if shop_exist.present?
       return_val = (shop_exist.subscription.present?) ? 1 : 0
    else
       return_val = 0
    end
   
    render :text => return_val
  end

  def comingSoon

    render layout: false
    puts "======>"+request.base_url
  end


  private
	def setting_params
	   #params.require(:setting).permit(:automatic_recommendations, :shop_id, :global_recommendations , :random_recommendations, :product_name, :product_description, :product_price, :add_to_cart,:activation,:quantity)
     params.require(:setting).permit(:shop_id, :is_bundle, :is_opt_in)
	end

  #def allow_iframe
    #response.headers.except! 'X-Frame-Options'
  #end
  
end
