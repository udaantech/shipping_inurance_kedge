class RecurringController < ApplicationController
   skip_before_action :verify_authenticity_token, :only => [:create]
   #before_action :load_current_recurring_charge

  def new
  end

  def create
    @root_url = request.base_url
    @shop_exist = Shop.where(:id => params[:shop_id]).first
    #start sessiom
    check_session(@shop_exist.id)

    #check previous subscription
    @recurring_application_charge = ShopifyAPI::RecurringApplicationCharge.current
    @recurring_application_charge.try!(:cancel)

    @recurring_application_charge = ShopifyAPI::RecurringApplicationCharge.new(
            name: "Shopify Product Insurance",
            price: 9.99,
            return_url:  @root_url+"/activatecharge/"+params[:shop_id],
            test: true,
            trial_days: 7,
            capped_amount: 100,
            terms: "$9.99 for every month")

    if @recurring_application_charge.save
      #@fullpage_redirect_to @recurring_application_charge.confirmation_url

      render :json => {:status => 'success',:url => @recurring_application_charge.confirmation_url}
    else
      #flash[:danger] = @recurring_application_charge.errors.full_messages.first.to_s.capitalize
      #redirect_to_correct_path(@recurring_application_charge)
       render :json => {:status => 'error',:message => @recurring_application_charge.errors.full_messages.first.to_s.capitalize}
    end
    
    #render :json => params

  end

  def activatecharge

    @shop_exist = Shop.where(:id => params[:id]).first
    #start sessiom
    check_session(@shop_exist.id)

    vars = request.query_parameters
    charge_id = (vars[:charge_id].present?) ? vars[:charge_id] : ''

    @recurring_application_charge = ShopifyAPI::RecurringApplicationCharge.find(charge_id)

    if @recurring_application_charge.status == 'accepted'
      @recurring_application_charge.activate
    end

    #create_order_webhook
    redirect_to_correct_path(@recurring_application_charge,params[:id])
    #redirect bulk_edit_url

    #render :json => params
  end

  def update
  end 

  def edit
  end

  def destroy
     @shop_exist = Shop.where(:id => params[:shop_id]).first
    #start sessiom
    check_session(@shop_exist.id)
    @recurring_application_charge = ShopifyAPI::RecurringApplicationCharge.find(params[:id])

    @recurring_application_charge.cancel

    flash[:success] = "Recurring application charge was cancelled successfully"

    redirect_to_correct_path(@recurring_application_charge,params[:shop_id])
  end

  def show
    @shop_exist = Shop.where(:id => params[:id]).first
    #start sessiom
    check_session(@shop_exist.id)

    @recurring_application_charge = ShopifyAPI::RecurringApplicationCharge.current
    @usage_charges = @recurring_application_charge.usage_charges if @recurring_application_charge.try(:capped_amount)
  end

  def index
  end

private

  def load_current_recurring_charge
    @recurring_application_charge = ShopifyAPI::RecurringApplicationCharge.current
  end

  def check_session(shop_id)
    session = ShopifyAPI::Session.new( @shop_exist.shopify_domain, @shop_exist.shopify_token)
    ShopifyAPI::Base.activate_session(session)
  end

  def recurring_application_charge_params
    params.require(:recurring_application_charge).permit(
      :name,
      :price,
      :capped_amount,
      :terms,
      :trial_days
    )
  end

  def redirect_to_correct_path(recurring_application_charge,shop_id)
    if recurring_application_charge.try(:capped_amount)
      #puts "====in if===>"
      redirect_to usage_charge_path(shop_id)
    else
      #puts "========in else======>"
      redirect_to recurring_application_charge_path
    end
  end


end
