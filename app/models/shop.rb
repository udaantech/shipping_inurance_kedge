class Shop < ActiveRecord::Base
  include ShopifyApp::SessionStorage

  has_one :setting, :dependent => :destroy

  #has_one :subscription
  has_one :subscription, -> { where status: 'accepted' }
end
