Rails.application.routes.draw do
	get 'recurring/new'

	#get 'recurring/create'
	post 'createbilling' => "recurring#create",as: 'createbilling'
	#get  'activatecharge/:id' => "recurring#activatecharge",as: 'activatecharge'
	get  'activatecharge/:id' => "home#activatecharge",as: 'activatecharge'
	get  'createnewrecurring' => "home#createnewrecurring",as: 'createnewrecurring'

	get 'recurring/update'

	get 'recurring/edit'

	#get 'recurring/destroy'
	delete 'destroyrecurring/:id/:shop_id' => "recurring#destroy",as: 'destroyrecurring'
	delete 'destroyrecurringhome/:id/:shop_id' => "home#destroy",as: 'destroyrecurringhome'

	#get 'recurring/show'
	get  'usage_charge/:id' => "recurring#show",as: 'usage_charge'

	get 'recurring/index'

	get 'settings/index'

	mount ShopifyApp::Engine, at: '/'
	get "auth/shopify/callback", to: 'home#callback'
	get "auth/shopify/login", to: 'home#index'
 	post 'setting' => "settings#create"

 	post 'webhookrequest' => "settings#webhookrequest"
 	get  'fetch_product' => "settings#fetch_product"
 	get  'check_shop_exist' => "settings#check_shop_exist"
 	get  'comingSoon' => "settings#comingSoon"

	post 'appUninstallWebHook' => "settings#appUninstallWebHook",as: 'appUninstallWebHook'

	post 'cartCreateWebHook' => "settings#cartCreateWebHook",as: 'cartCreateWebHook'

	post 'cartUpdateWebHook' => "settings#cartUpdateWebHook",as: 'cartUpdateWebHook'

	#routes for recurring path
	get  'decline_charge/:id' => "home#decline_charge",as: 'decline_charge'
	get  'all_orders/' => "home#all_orders",as: 'all_orders'

	root :to => 'home#index'
	#root :to => 'settings#comingSoon'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
