ShopifyApp.configure do |config|
  config.application_name = "Shipping Insurance Shopify"
  #development
  #config.api_key = "83fe89822855176157b41bc853431faf"
  #config.secret = "b1eb993e9d3a58d4d7d3ba73b877c705"

  #production
  config.api_key = "ba0c0f3b69f9b84f5a160ccc9e57132a"
  config.secret = "b501255a24d91875187b45c3a9ebf512"

  config.scope = "read_orders, read_products, write_products, read_themes, write_themes"
  config.embedded_app = true
  config.after_authenticate_job = false
  config.session_repository = Shop
end
