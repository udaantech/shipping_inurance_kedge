require 'test_helper'

class RecurringControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get recurring_new_url
    assert_response :success
  end

  test "should get create" do
    get recurring_create_url
    assert_response :success
  end

  test "should get update" do
    get recurring_update_url
    assert_response :success
  end

  test "should get edit" do
    get recurring_edit_url
    assert_response :success
  end

  test "should get destroy" do
    get recurring_destroy_url
    assert_response :success
  end

  test "should get show" do
    get recurring_show_url
    assert_response :success
  end

  test "should get index" do
    get recurring_index_url
    assert_response :success
  end

end
