var shop_name = document
  .querySelector('script[data-id="boughtshopId"][data-shop]')
  .getAttribute('data-shop');

var host_url = document
  .querySelector('script[data-id="boughtshopId"][data-requesturl]')
  .getAttribute('data-requesturl');

var bundled_variant_id = document
  .querySelector('script[data-id="boughtshopId"][data-bundled_variant_id]')
  .getAttribute('data-bundled_variant_id');

var bundled_opt_in_id = document
  .querySelector('script[data-id="boughtshopId"][data-bundled_opt_in_id]')
  .getAttribute('data-bundled_opt_in_id');

var is_bundle = document
  .querySelector('script[data-id="boughtshopId"][data-is_bundle]')
  .getAttribute('data-is_bundle');

var is_opt_in = document
  .querySelector('script[data-id="boughtshopId"][data-is_opt_in]')
  .getAttribute('data-is_opt_in');

$( document ).ready(function() {

   var temp = [];
   var temp_opt = [];
   var is_change_quantity = false;
    //if bundle setting enable
    if(is_bundle == 1) {
     
      Shopify.getCart(function(data){
         var check_cart_is_empty = data.items.length;
         //if check cart empty
         if(check_cart_is_empty > 0) {
          //first add the product
          //Shopify.addItem(bundled_variant_id, 1);
          for (i=0;i<data.items.length;i++) { 
            var x = data.items[i];
            var variantid = x.variant_id;
            temp[i] = variantid.toString();
          } // end for loop
          
         } //end if

         
         var is_insured_product_exist = temp.includes(bundled_variant_id.toString());
         //console.log("======temp===>",temp);
         if(is_insured_product_exist) {

            if( (temp.length == 1 ) && (temp[0].toString() == bundled_variant_id.toString())) { 
              Shopify.removeItem(bundled_variant_id);

              setTimeout(function(){
               window.location.reload();
              },1000);

            } else {
              Shopify.changeItem(bundled_variant_id, 1);  
            }
            
           
         } else {
           if( (temp.length>0 ) && (temp[0].toString() != bundled_variant_id.toString())) {
            //console.log('==========in if============');
            Shopify.addItem(bundled_variant_id, 1);
            //location.reload();
            setTimeout(function(){
             window.location.reload();
            },1000);

           }
           
         }

      }); //get cart end



    }//end if bundle setting enable


    //if is_opt_in setting enabled
    if(is_opt_in == 1) {
      //hide checkbox
      var check_check_box_checked = localStorage.getItem("is_checkbox_checked");
     
      if (check_check_box_checked == 'yes') {
        $('#opt_in_chk_lbl').show(); 
        $('#opt_in_chk').prop('checked', true);  
      } else if(check_check_box_checked == 'no') {
        $('#opt_in_chk_lbl').show(); 
        $('#opt_in_chk').prop('checked', false);  
      } else {
        $('#opt_in_chk_lbl').show();  
        $('#opt_in_chk').prop('checked', true); 
      }
      
      //later on we can do it dynamic these two values
      var opt_in_percentage= 1.5;
      var opt_in_product_price = 0.01;

      //check chekbox is checked on page load
      if($('#opt_in_chk').is(':checked')) {
        //get card
        Shopify.getCart(function(data){

        var check_cart_is_empty = data.items.length;
        //if check cart empty
        if(check_cart_is_empty > 0) {
          //first add the product
          //Shopify.addItem(bundled_variant_id, 1);
          var total_price=0;
          for (i=0;i<data.items.length;i++) { 
            var x = data.items[i];
            var variantid = x.variant_id;
            var  item_price = x.line_price;
            if(variantid != bundled_opt_in_id) {
             total_price = parseFloat(Shopify.formatMoney(item_price).substr(1)) + parseFloat(total_price);
            }

            temp_opt[i] = variantid.toString();
          } // end for loop


        } //end if


        var cart_total = (total_price*opt_in_percentage)/100;
        cart_total = cart_total*100;
        cart_total = cart_total.toFixed(2);
        cart_total = parseInt(cart_total);
        //cart_total = Math.ceil(cart_total);
       
        var is_insured_product_exist = temp_opt.includes(bundled_opt_in_id.toString());
         //alert(is_insured_product_exist);
         //console.log("======temp===>",temp);
         if(is_insured_product_exist) {

            if( (temp_opt.length == 1 ) && (temp_opt[0].toString() == bundled_opt_in_id.toString())) { 
              Shopify.removeItem(bundled_opt_in_id);
              
              setTimeout(function(){
               window.location.reload();
              },1000);

            } else {
              is_change_quantity = true;
              Shopify.changeItem(bundled_opt_in_id, cart_total);

            }
            
           
         } else {
           if( (temp_opt.length>0 ) && (temp_opt[0].toString() != bundled_opt_in_id.toString())) {
            //console.log('==========in if============');
            Shopify.addItem(bundled_opt_in_id, cart_total);
            //location.reload();
            setTimeout(function(){
             window.location.reload();
            },1000);

           }
           
         }


      });//end to get cart


      } else {
          if( (temp_opt.length == 1 ) && (temp_opt[0].toString() == bundled_opt_in_id.toString())) { 
              Shopify.removeItem(bundled_opt_in_id);
              
              setTimeout(function(){
               window.location.reload();
              },1000);

            }

      }


      //start code if checkbox clicked
      $('#opt_in_chk').click( function() {
        if($(this).is(':checked')) {
          //alert('checked');

          localStorage.setItem("is_checkbox_checked", "yes");

          //get card
            setTimeout(function(){
             window.location.reload();
            },1000);


        } else { //else uncheck
          localStorage.setItem("is_checkbox_checked", "no");
          Shopify.removeItem(bundled_opt_in_id);
          setTimeout(function(){
            window.location.reload();
          },1000);
        }
      }); //end check if checbox click
      
    }//end if is_opt_in setting enabled


    
});