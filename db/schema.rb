# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181214080123) do

  create_table "associates", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "product_main_id"
    t.string   "product_combination_id"
    t.integer  "weight"
    t.integer  "order_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "order_items", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "title"
    t.integer  "order_id"
    t.integer  "variant_id"
    t.integer  "quantity"
    t.integer  "price"
    t.integer  "sku"
    t.string   "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "shop_id"
    t.string   "shopify_store_name"
    t.string   "creator_id"
    t.string   "order_id"
    t.decimal  "total_price",            precision: 10, scale: 2
    t.decimal  "total_tax",              precision: 10, scale: 2
    t.decimal  "total_shipping_cost",    precision: 10, scale: 2
    t.decimal  "total_shipping_charged", precision: 10, scale: 2
    t.string   "order_number"
    t.string   "status"
    t.datetime "created"
    t.datetime "updated"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "currency"
    t.decimal  "subtotal_price",         precision: 10, scale: 2
    t.string   "gateway"
    t.string   "shopify_order_id"
    t.string   "customer_id"
    t.string   "customer_first_name"
    t.string   "customer_last_name"
    t.string   "customer_phone"
    t.string   "customer_email"
    t.string   "customer_city"
    t.string   "customer_country"
    t.string   "customer_zip"
    t.string   "insured"
  end

  create_table "settings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "shopify_store_id",   limit: 11
    t.string   "shopify_store_name", limit: 100
    t.integer  "activation"
    t.datetime "created"
    t.datetime "updated"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.integer  "shop_id"
    t.integer  "is_bundle",                      default: 0
    t.integer  "is_opt_in",                      default: 0
    t.string   "bundled_variant_id"
    t.string   "opt_in_variant_id"
    t.integer  "opt_in_value"
  end

  create_table "shoplogs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "shop_domain"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "shops", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "shopify_domain", null: false
    t.string   "shopify_token",  null: false
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["shopify_domain"], name: "index_shops_on_shopify_domain", unique: true, using: :btree
  end

  create_table "subscriptions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "shop_id"
    t.string   "subscription_type"
    t.string   "status"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

end
