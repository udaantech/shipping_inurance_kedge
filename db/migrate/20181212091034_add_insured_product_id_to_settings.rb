class AddInsuredProductIdToSettings < ActiveRecord::Migration[5.0]
  def change
  	add_column :settings, :bundled_variant_id, :string, default: nil
  	add_column :settings, :opt_in_variant_id, :string, default: nil
  	add_column :settings, :opt_in_value, :integer, default: nil
  end
end
