class CreateSubscriptions < ActiveRecord::Migration[5.0]
  def change
    create_table :subscriptions do |t|
      t.integer :shop_id
      t.string :subscription_type
      t.string :status

      t.timestamps
    end
  end
end
