class AddcolumnsToOrders < ActiveRecord::Migration[5.0]
  def change
  	add_column :orders, :currency, :string, default: nil
  	add_column :orders, :subtotal_price, :decimal, precision: 10, scale: 2,null: true
  	add_column :orders, :gateway, :string, default: nil
  	add_column :orders, :shopify_order_id, :string, default: nil
  	add_column :orders, :customer_id, :string, default: nil
  	add_column :orders, :customer_first_name, :string, default: nil
  	add_column :orders, :customer_last_name, :string, default: nil
  	add_column :orders, :customer_phone, :string, default: nil
  	add_column :orders, :customer_email, :string, default: nil
  	add_column :orders, :customer_city, :string, default: nil
  	add_column :orders, :customer_country, :string, default: nil
  	add_column :orders, :customer_zip, :string, default: nil
  	add_column :orders, :insured, :string, default: nil
  end
end
