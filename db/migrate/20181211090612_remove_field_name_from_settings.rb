class RemoveFieldNameFromSettings < ActiveRecord::Migration[5.0]

	def change
	  	remove_column :settings, :automatic_recommendations
	  	remove_column :settings, :global_recommendations
	  	remove_column :settings, :random_recommendations
	  	remove_column :settings, :product_name
	  	remove_column :settings, :product_description
	  	remove_column :settings, :product_price
	  	remove_column :settings, :add_to_cart
	  	remove_column :settings, :quantity
	end

end
