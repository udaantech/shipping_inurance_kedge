class CreateAssociates < ActiveRecord::Migration[5.0]
  def change
    create_table :associates do |t|
      t.string :product_main_id
      t.string :product_combination_id
      t.integer :weight
      t.integer :order_id

      t.timestamps
    end
  end
end
