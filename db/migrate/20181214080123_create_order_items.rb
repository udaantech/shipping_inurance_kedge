class CreateOrderItems < ActiveRecord::Migration[5.0]
  def change
    create_table :order_items do |t|
      t.string :title
      t.integer :order_id,null: true
      t.integer :variant_id,null: true 
      t.integer :quantity,null: true
      t.integer :price,precision: 10, scale: 2,null: true
      t.integer :sku,null: true
      t.string :product_id,null: true
      t.timestamps
    end
  end
end
