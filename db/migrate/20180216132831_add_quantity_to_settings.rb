class AddQuantityToSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :settings, :quantity, :integer, default: 10
  end
end
