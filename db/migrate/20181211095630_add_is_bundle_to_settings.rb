class AddIsBundleToSettings < ActiveRecord::Migration[5.0]
  def change
  	add_column :settings, :is_bundle, :integer, default: 0
	add_column :settings, :is_opt_in, :integer, default: 0
  end
end
