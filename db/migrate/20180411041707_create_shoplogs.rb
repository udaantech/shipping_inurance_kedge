class CreateShoplogs < ActiveRecord::Migration[5.0]
  def change
    create_table :shoplogs do |t|
      t.string :shop_domain

      t.timestamps
    end
  end
end
