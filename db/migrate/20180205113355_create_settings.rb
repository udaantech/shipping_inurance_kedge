class CreateSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :settings do |t|
      t.integer :shopify_store_id,null: true
      t.integer :shopify_store_name,null: true
      t.integer :activation,null: true
      t.integer :recommendations,null: true
      t.string :product_name,null: true
      t.string :product_description, limit: 400,null: true
      t.decimal :product_price,precision: 10, scale: 2,null: true
      t.decimal :product_price,precision: 10, scale: 2,null: true
      t.integer :add_to_cart,null: true
      t.datetime :created,null: true
      t.datetime :updated,null: true

      t.timestamps
    end
  end
end
