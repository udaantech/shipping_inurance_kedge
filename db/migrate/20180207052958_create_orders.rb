class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.integer :shop_id,null: true
      t.string :shopify_store_name,null: true
      t.string :creator_id,null: true
      t.string :order_id,null: true
      t.decimal :total_price,precision: 10, scale: 2,null: true
      t.decimal :total_price,precision: 10, scale: 2,null: true
      t.decimal :total_tax,precision: 10, scale: 2,null: true
      t.decimal :total_tax,precision: 10, scale: 2,null: true
      t.decimal :total_shipping_cost,precision: 10, scale: 2,null: true
      t.decimal :total_shipping_cost,precision: 10, scale: 2,null: true
      t.decimal :total_shipping_charged,precision: 10, scale: 2,null: true
      t.string :order_number,null: true
      t.string :status,null: true
      t.datetime :created,null: true
      t.datetime :updated,null: true

      t.timestamps
    end
  end
end
